<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Http\Enums\RolesEnum;

class Admin
{

    public function handle($request, Closure $next)
    {

        if (Auth::check() && (Auth::user()->role_id === RolesEnum::ADMIN)) {
            return $next($request);
        }

        return redirect('home');

    }

}