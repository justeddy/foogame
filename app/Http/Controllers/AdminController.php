<?php

namespace App\Http\Controllers;

use App\Http\Models\Classes;
use App\Http\Models\Database;
use App\Http\Models\Roles;
use App\Http\Models\Students;
use App\Http\Models\Users;
use App\UploadFile;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use App\Http\Models\Problems;

class AdminController extends Controller
{

    /**
     * AdminController constructor.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

}
