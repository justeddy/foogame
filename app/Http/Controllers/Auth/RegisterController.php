<?php

namespace App\Http\Controllers\Auth;

use App\Http\Models\Teams;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'club_name' => 'required|string|max:255|unique:users',
            'short_club_name' => 'required|string|max:3',
            'team_name' => 'required|string|max:30|unique:teams',
            'short_team_name' => 'required|string|max:3',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'club_name' => $data['club_name'],
            'short_club_name' => $data['short_club_name'],
        ]);

        $data['account_id'] = $user->id;

        Teams::createTeam([
            'team_name' => $data['team_name'],
            'short_team_name' => $data['short_team_name'],
            'account_id' => $data['account_id'],
        ]);

        return $user;
    }
}

