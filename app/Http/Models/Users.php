<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\DB;

class Users
{
    const TABLENAME = 'users';

    public static function insert($data)
    {
        DB::table(self::TABLENAME)->insert([
            'username' => $data->username,
            'email' => $data->email,
            'password' => bcrypt($data->password),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'role_id' => $data->accountType
        ]);
    }

}