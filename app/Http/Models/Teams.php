<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\DB;

class Teams
{
    const TABLENAME = 'teams';

    public static function createTeam($data)
    {
        DB::table(self::TABLENAME)->insert([
            'team_name' => $data['team_name'],
            'short_team_name' => $data['short_team_name'],
            'found_date' => date('Y-m-d'),
            'account_id' => $data['account_id']
        ]);
    }

}