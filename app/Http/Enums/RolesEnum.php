<?php

namespace App\Http\Enums;

class RolesEnum
{
    const ADMIN = 1;
    const MODERATOR = 2;
    const PLAYER = 3;
}