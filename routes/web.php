<?php

use Illuminate\Support\Facades\Auth;

Auth::routes();

$this->get('/', 'HomeController@index');
$this->get('/home', 'HomeController@index')->name('homeRoute');
$this->get('/logout', 'Auth\LoginController@logout');
$this->post('/register', 'Auth\RegisterController@register');
