<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/images/davinci-logo.png") }}" class="img-circle"
                     alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>{{ \Illuminate\Support\Facades\Auth::user()->name }}</p>
                <a href="#">
                    <i class="fa fa-circle text-success"></i>
                    Online
                </a>
            </div>
        </div>
        <form class="sidebar-form">
            <div class="input-group">
                <input autocomplete="off" id="searchBar" type="text" class="form-control" placeholder="Search"/>
                <span class="input-group-btn">
                    <a id='search-btn' class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </a>
                </span>
            </div>
            <div id="dropdown-content">

            </div>
        </form>
        <ul class="sidebar-menu">
            <li class="header">Menu</li>
            <li>
                <a href="{{ route('homeRoute') }}" class="startLoadingButton"><i class="fa fa-home"></i>Dashboard</a>
            </li>
            @if(\Illuminate\Support\Facades\Auth::user()->role_id == \App\Http\Enums\RolesEnum::ADMIN)
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-lock"></i>
                        Admin Paneel
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">

                    </ul>
                </li>
            @endif
        </ul>

        <ul class="sidebar-menu-overview">
            <li>Club: {{ \Illuminate\Support\Facades\Auth::user()->club_name }}</li>
            <li>Money: &euro;{{ \Illuminate\Support\Facades\Auth::user()->money }}</li>
            <li>Fans: {{ \Illuminate\Support\Facades\Auth::user()->fans }}</li>
            <li>Reputation club: {{ \Illuminate\Support\Facades\Auth::user()->club_reputation }} (max: {{ \App\Http\Enums\NamesEnum::MAXREPUTATIONCLUB }}) </li>
        </ul>
    </section>
</aside>