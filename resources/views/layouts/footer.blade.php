<footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>
        <a href="#">{{ \App\Http\Enums\NamesEnum::PROJECTNAME }}</a>
    </strong>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
</footer>