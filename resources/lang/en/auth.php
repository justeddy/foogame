<?php

return [
    'failed' => 'Uw gebruikersnaam en/of wachtwoord is incorrect.',
    'throttle' => 'Te veel mislukte inlogpogingen. Probeer het opnieuw in :seconds seconden.',
];
