$(window).load(function () {
    $('input.empty').val('');
    checkAlerts();
    $("#manoBold").on('click', function () {
        document.location = '/login';
    });
    $("#logoutUser").on("click", function () {
        document.location = '/logout';
    });

    $('#addAccountForm').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var empty = form.find(".required").filter(function () {
            return this.value === "";
        });
        if (!empty.length) {
            loadingGif();
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize()
            }).done(function (result) {
                if (result !== false) {
                    if (result == 'password') {
                        localStorage.setItem('addAccountPassword', 'false');
                        location.reload();
                    } else {
                        localStorage.setItem('addAccount', 'true');
                        location.reload();
                    }
                } else {
                    localStorage.setItem('addAccount', 'false');
                    location.reload();
                }
            });
        } else {
            form.find('.required').each(function () {
                if (!this.value) {
                    $(this).css('border', '1px solid red');
                    $(this).parent().find('.alertHide').show();
                } else if (this.value) {
                    $(this).css('border', '1px solid grey');
                    $(this).parent().find('.alertHide').hide();
                }
            });
        }
    });

    $('.startLoadingButton').on('click', function () {
        loadingGif();
    });
});

function loadingGif() {
    var container = $('.container');
    var contentBackground = $('.contentBackground');
    $('.loadingGif').show();
    container.addClass('blurBackground');
    contentBackground.addClass('blurBackground');
    container.css('pointer-events', 'none');
    contentBackground.css('pointer-events', 'none');
}

function loadingGifBox() {
    var boxBody = $('.chart-loading');
    $('.loadingGifBox').show();
    boxBody.addClass('blurBackground');
}

function removeLoadingGifBox() {
    var boxBody = $('.chart-loading');
    $('.loadingGifBox').hide();
    boxBody.removeClass('blurBackground');
}

function checkAlerts() {
    var addClass = localStorage.getItem('addClass');
    var addAccount = localStorage.getItem('addAccount');
    var addAccountPassword = localStorage.getItem('addAccountPassword');
    var roleAlert = $('.roleSuccessAlert');
    var deleteStudents = localStorage.getItem('deleteStudents');

    if (addAccount == 'true') {
        $('.accountSuccessAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addAccount', null);
    } else if (addAccount == 'false') {
        $('.accountErrorAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addAccount', null);
    }

    if (addAccountPassword == 'false') {
        $('.accountPasswordErrorAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addAccountPassword', null);
    }

    if (addClass == 'true') {
        $('.classSuccessAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addClass', null)
    } else if (addClass == 'false') {
        $('.classErrorAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('addClass', null)
    }
    if (roleAlert.length) {
        roleAlert.delay(3000).fadeOut('slow');
    }

    if (deleteStudents == 'true') {
        $('.deleteStudentsSuccessAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('deleteStudents', null);
    } else if (deleteStudents == 'false') {
        $('.deleteStudentsErrorAlert').fadeIn().delay(2000).fadeOut('slow');
        localStorage.setItem('deleteStudents', null);
    }
}

$(function () {
    $('input.studentRow, input.rememberMeBox').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
});

$(function () {
    var languageDataTable =
        'Aantal resultaten <select>' +
        '<option value="10">10</option>' +
        '<option value="25">25</option>' +
        '<option value="50">50</option>' +
        '<option value="100">100</option>' +
        '<option value="-1">Alles</option>' +
        '</select>';

    $('#studentsTable, #problemsTable, #classTable').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "columnDefs": [
            {orderable: false, targets: 'noSort'},
            {type: 'date-eu', targets: 'dateSort'}
        ],
        "aaSorting": [],
        "language": {
            "lengthMenu": languageDataTable,
            "sSearch": "Zoeken: ",
            "sInfoEmpty": "Er worden 0 tot 0 van de 0 resulaten weergegeven",
            "sInfo": "Er worden _START_ tot _END_ van de _TOTAL_ resulaten weergegeven",
            "sInfoFiltered": "(totaal _MAX_ resultaten)",
            "sZeroRecords": "Geen resultaten gevonden",
            paginate: {
                next: '&#8594;',
                previous: '&#8592;'
            }
        }
    });
});