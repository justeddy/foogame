var studentSignalPoints;
var classesSignalPoints;

function makeSignalPointsChart(classId) {
    var signalPointsChart = $("#signalPointsChart");
    if (signalPointsChart.length) {
        $.ajax({
            url: "/admin/getSignalPoints/" + classId
        }).done(function (result) {
            var signalPoints = [];
            var students = result;
            var studentData = {
                labels: [],
                datasets: [
                    {
                        label: "",
                        fillColor: "rgba(210, 214, 222, 1)",
                        strokeColor: "rgba(210, 214, 222, 1)",
                        pointColor: "rgba(210, 214, 222, 1)",
                        pointStrokeColor: "#c1c7d1",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: []
                    }
                ]
            };
            for (var i = 0; i < students.length; i++) {
                signalPoints.push(students[i].signal_points).toString();
                studentData.labels.push(students[i].name);
                studentData.datasets[0].data = signalPoints;
            }
            var barChartCanvas = $(signalPointsChart).get(0).getContext("2d");
            studentSignalPoints = new Chart(barChartCanvas);
            var barChartData = studentData;
            barChartData.datasets[0].fillColor = "#00a65a";
            barChartData.datasets[0].strokeColor = "#00a65a";
            barChartData.datasets[0].pointColor = "#00a65a";
            var barChartOptions = {
                animation: true,
                scaleBeginAtZero: true,
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                scaleShowHorizontalLines: true,
                scaleShowVerticalLines: true,
                barShowStroke: true,
                barStrokeWidth: 2,
                barValueSpacing: 5,
                barDatasetSpacing: 1,
                legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                responsive: true,
                maintainAspectRatio: true
            };
            barChartOptions.datasetFill = false;
            studentSignalPoints.Bar(barChartData, barChartOptions);
        });
    }
}

function makeClassesChart() {
    var canvas = $('.signalPointsAllClasses');
    loadingGifBox();
    $.ajax({
        url: "/admin/getAllSignalPoints/"
    }).done(function (result) {
        var signalPoints = [];
        var classes = result;
        var classesData = {
            labels: [],
            datasets: [
                {
                    label: "",
                    fillColor: "rgba(210, 214, 222, 1)",
                    strokeColor: "rgba(210, 214, 222, 1)",
                    pointColor: "rgba(210, 214, 222, 1)",
                    pointStrokeColor: "#c1c7d1",
                    pointHighlightFill: "#fff",
                    pointHighlightStroke: "rgba(220,220,220,1)",
                    data: []
                }
            ]
        };
        $.each(classes, function () {
            if (this.signal_points >= 0) {
                signalPoints.push(this.signal_points).toString();
                classesData.labels.push(this.class_code);
                classesData.datasets[0].data = signalPoints;
            }
        });
        var barChartCanvas = $(canvas).get(0).getContext("2d");
        classesSignalPoints = new Chart(barChartCanvas);
        var barChartData = classesData;
        barChartData.datasets[0].fillColor = "#00a65a";
        barChartData.datasets[0].strokeColor = "#00a65a";
        barChartData.datasets[0].pointColor = "#00a65a";
        var barChartOptions = {
            animation: true,
            scaleBeginAtZero: true,
            scaleShowGridLines: true,
            scaleGridLineColor: "rgba(0,0,0,.05)",
            scaleGridLineWidth: 1,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: true,
            barShowStroke: true,
            barStrokeWidth: 2,
            barValueSpacing: 5,
            barDatasetSpacing: 1,
            legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
            responsive: true,
            maintainAspectRatio: true
    };
        barChartOptions.datasetFill = false;
        classesSignalPoints.Bar(barChartData, barChartOptions);
        removeLoadingGifBox();
    });
}

function makeAssingmentsChart() {
    var areaChartCanvas = $("#assignmentsChart").get(0).getContext("2d");
    var areaChart = new Chart(areaChartCanvas);
    var areaChartData = {
        labels: ["Openstaand", "Ingeleverd", "Afgekeurd", "Afgerond"],
        datasets: [
            {
                label: "",
                fillColor: "rgba(60,141,188,0.9)",
                strokeColor: "rgba(60,141,188,0.8)",
                pointColor: "#3b8bba",
                pointStrokeColor: "rgba(60,141,188,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data: [65, 59, 80, 81, 56, 55, 40]
            }
        ]
    };

    var areaChartOptions = {
        showScale: true,
        scaleShowGridLines: false,
        scaleGridLineColor: "rgba(0,0,0,.05)",
        scaleGridLineWidth: 1,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        bezierCurve: true,
        bezierCurveTension: 0.3,
        pointDot: false,
        pointDotRadius: 4,
        pointDotStrokeWidth: 1,
        pointHitDetectionRadius: 20,
        datasetStroke: true,
        datasetStrokeWidth: 2,
        datasetFill: true,
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        maintainAspectRatio: true,
        responsive: true
    };

    areaChart.Line(areaChartData, areaChartOptions);
}