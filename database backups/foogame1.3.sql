-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 23 jan 2018 om 11:45
-- Serverversie: 5.7.14
-- PHP-versie: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `foogame`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `injuries`
--

CREATE TABLE `injuries` (
  `id` int(11) NOT NULL,
  `inury` varchar(256) NOT NULL,
  `duration` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `injuries`
--

INSERT INTO `injuries` (`id`, `inury`, `duration`) VALUES
(1, 'Beenwond', 4);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `injury_list`
--

CREATE TABLE `injury_list` (
  `id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `type_injury_id` int(11) NOT NULL,
  `start_injury` date NOT NULL,
  `end_injury` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `injury_list`
--

INSERT INTO `injury_list` (`id`, `player_id`, `type_injury_id`, `start_injury`, `end_injury`) VALUES
(1, 1, 1, '2017-12-08', '2017-12-12');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `players`
--

CREATE TABLE `players` (
  `id` int(11) NOT NULL,
  `player_name` varchar(50) NOT NULL,
  `length` int(4) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `born_date` date NOT NULL COMMENT 'Centimeters',
  `convinded` int(5) NOT NULL,
  `agressive` int(5) NOT NULL,
  `leadership` int(5) NOT NULL,
  `team_spirit` int(5) NOT NULL,
  `pace` int(5) NOT NULL,
  `passes` int(5) NOT NULL,
  `dribbel` int(5) NOT NULL,
  `shooting` int(5) NOT NULL,
  `header` int(5) NOT NULL,
  `tackle` int(5) NOT NULL,
  `left_foot` int(5) NOT NULL,
  `right_foot` int(5) NOT NULL,
  `free_kicks` int(5) NOT NULL,
  `corner_kicks` int(5) NOT NULL,
  `jumping` int(5) NOT NULL,
  `diving` int(5) NOT NULL,
  `goalkicks` int(5) NOT NULL,
  `reflexes` int(5) NOT NULL,
  `boss_penalty_area` int(5) NOT NULL,
  `position_id` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL,
  `position` varchar(255) NOT NULL,
  `short_position_name` varchar(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `positions`
--

INSERT INTO `positions` (`id`, `position`, `short_position_name`) VALUES
(1, 'Goalkeeper', 'GK'),
(2, 'Central defender', 'CB'),
(4, 'Left back', 'LB'),
(5, 'Right back', 'RB'),
(6, 'Defending midfielder', 'DM'),
(7, 'Central midfielder', 'CM'),
(8, 'Left midfielder', 'LM'),
(9, 'Right midfielder', 'RM'),
(10, 'Left wing', 'LW'),
(11, 'Right wing', 'RW'),
(12, 'Attacking midfielder', 'AM'),
(13, 'Central forward', 'CF');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `roles` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `roles`
--

INSERT INTO `roles` (`id`, `roles`) VALUES
(1, 'Owner'),
(2, 'Admin'),
(3, 'Moderator'),
(4, 'Player');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `team_name` varchar(30) NOT NULL,
  `short_team_name` varchar(3) NOT NULL,
  `reputation` varchar(255) NOT NULL DEFAULT '10',
  `found_date` date NOT NULL,
  `account_id` int(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `teams`
--

INSERT INTO `teams` (`id`, `team_name`, `short_team_name`, `reputation`, `found_date`, `account_id`) VALUES
(1, 'Knudde 1', 'K1', '764', '2017-12-13', 1),
(4, 'vvgz 1', 'vg1', '10', '2018-01-13', 28),
(5, 'fjfn', 'o', '10', '2018-01-17', 29),
(6, 'Boterletters', 'BOT', '10', '2018-01-17', 30);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL,
  `club_name` varchar(40) CHARACTER SET latin1 DEFAULT NULL,
  `fans` varchar(255) CHARACTER SET latin1 DEFAULT '500',
  `money` varchar(255) CHARACTER SET latin1 DEFAULT '500000',
  `short_club_name` varchar(3) CHARACTER SET latin1 DEFAULT NULL,
  `club_logo` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `role_id` int(11) NOT NULL DEFAULT '4',
  `club_reputation` int(11) NOT NULL DEFAULT '50'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `club_name`, `fans`, `money`, `short_club_name`, `club_logo`, `role_id`, `club_reputation`) VALUES
(1, 'el', 'eddydelange@live.nl', '$2y$10$4Msa3teVvhiMUIz9fYuJQemySK8gbNSxFc3h4R1fgjwmP66zeGnOq', '', '2017-04-20 05:29:31', '2017-04-20 05:29:31', 'FC Knudde', '49385', '300000', 'FCK', 'clubje.jpg', 1, 50),
(28, 'JustEddy', 'eddygames@live.nl', '$2y$10$zSvpRTZcG7/Xf9zwA6waxOV/vz5YUTcJdyJOFrYPdOMyrDI18SE3a', NULL, '2018-01-13 11:06:05', '2018-01-13 11:06:05', 'vvgz', '500', '500000', 'vgz', NULL, 4, 50),
(30, 'Harry', 'potter@zweinstein.uk', '$2y$10$O7TSyT7UZ3EE2A2LgZLeQeCieTcXaBxtRre8SoWGpd5VBe8Z9rZni', NULL, '2018-01-17 09:55:18', '2018-01-17 09:55:18', 'Tovervlaclub', '500', '500000', 'TOV', NULL, 4, 50);

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `injuries`
--
ALTER TABLE `injuries`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `injury_list`
--
ALTER TABLE `injury_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexen voor tabel `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `injuries`
--
ALTER TABLE `injuries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `injury_list`
--
ALTER TABLE `injury_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT voor een tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `players`
--
ALTER TABLE `players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT voor een tabel `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
